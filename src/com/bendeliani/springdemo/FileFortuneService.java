package com.bendeliani.springdemo;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by BG on 21-Feb-17.
 */

@Component
public class FileFortuneService implements FortuneService {

    private String fileName = "C:\\Users\\BG\\Desktop\\spring projects\\spring-demo-annotations\\src\\fortune-data.txt";
    private List<String> fortunes;

    private Random myRandom = new Random();

    public FileFortuneService(){
        System.out.println("Inside FileFortuneService default constructor");
    }

    // loading file information
    @PostConstruct
    public void loadFortunesFile() {
        System.out.println("building FileFortuneService");

        File theFile = new File(fileName);

        System.out.println("Reading fortunes from file: " + fileName);

        // initialize arrayList
        fortunes = new ArrayList<String>();

        // read fortunes from file
        try(BufferedReader reader = new BufferedReader(new FileReader(theFile))) {

            String line;

            while((line = reader.readLine())!=null) {
                fortunes.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getDailyFortune() {

        // pick a random string from the array
        int index = myRandom.nextInt(fortunes.size());

        return fortunes.get(index);
    }
}
