package com.bendeliani.springdemo;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by BG on 21-Feb-17.
 */

@Component
public class RandomFortuneService implements FortuneService {

    // create an array of Strings
    private String[] data = {
            "Bad luck",
            "you'll success today",
            "You have a normal day"
    };

    // create random number generator
    private Random myRandom = new Random();

    @Override
    public String getDailyFortune() {

       // pick a random String from the array
        int index = myRandom.nextInt(data.length);

        String theFortune = data[index];
        return theFortune;
    }
}
