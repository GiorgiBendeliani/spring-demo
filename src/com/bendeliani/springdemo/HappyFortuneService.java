package com.bendeliani.springdemo;

import org.springframework.stereotype.Component;

/**
 * Created by BG on 20-Feb-17.
 */

@Component
public class HappyFortuneService implements FortuneService {
    @Override
    public String getDailyFortune() {
        return "Today is your lucky day!!";
    }
}
