package com.bendeliani.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by BG on 20-Feb-17.
 */
public class SwimJavaConfigDemoApp {
    public static void main(String[] args) {

        // read spring config file
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SportConfig.class);

        // get the bean from spring container
        SwimCoach theCoach = context.getBean("swimCoach", SwimCoach.class);


        // call a method on the bean
        System.out.println(theCoach.getDailyWorkout());

        // call method to get dailyFortune
        System.out.println(theCoach.getDailyFortune());

        // testing new swimCoach methods
        System.out.println("Email: " + theCoach.getEmail());
        System.out.println("Team: " + theCoach.getTeam());

        // close the context
        context.close();

    }
}
