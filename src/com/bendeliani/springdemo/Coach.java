package com.bendeliani.springdemo;

/**
 * Created by BG on 20-Feb-17.
 */
public interface Coach {
    public String getDailyWorkout();

    public String getDailyFortune();
}
