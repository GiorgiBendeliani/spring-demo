package com.bendeliani.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by BG on 21-Feb-17.
 */

@Configuration
//@ComponentScan("com.bendeliani.springdemo")
@PropertySource("classpath:sport.properties")
public class SportConfig {

    // define a bean for sadFortuneService
    @Bean
    public FortuneService sadFortuneService() {
        return new SadFortuneService();
    }

    // define a bean for SwimCoach and inject dependency
    @Bean
    public Coach swimCoach(){
        return new SwimCoach(sadFortuneService());
    }



}
