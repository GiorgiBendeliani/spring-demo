package com.bendeliani.springdemo;

/**
 * Created by BG on 20-Feb-17.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
//@Scope("prototype")
public class TennisCoach implements Coach {

    @Autowired
    @Qualifier("fileFortuneService")
    private FortuneService fortuneService;

    /*@Autowired
    public TennisCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }*/

    public TennisCoach(){
        System.out.println(">> inside tennisCoach default constructor");
    }

    // define init method
    @PostConstruct
    public void doMyStartupStuff() {
        System.out.println("Inside: doMyStartupStuff method");
    }

    //define destroy method
    @PreDestroy
    public void doMyCleanupStuff() {
        System.out.println("Inside: doMyCleanupStuff");
    }

   /* @Autowired
    public void setFortuneService(FortuneService fortuneService) {
        System.out.println(">>inside setFortuneService method");
        this.fortuneService = fortuneService;
    }*/

    @Override
    public String getDailyWorkout() {
        return "Practise you backhand volley";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getDailyFortune();
    }
}
