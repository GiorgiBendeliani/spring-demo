package com.bendeliani.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by BG on 21-Feb-17.
 */
public class AnnotationBeanScopeDemoApp {
    public static void main(String[] args) {

        // load spring config file
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        // retrieve bean from spring container
        Coach theCoach = context.getBean("tennisCoach", Coach.class);

        Coach alphaCoach = context.getBean("tennisCoach", Coach.class);

        // check if the beans are the same
        boolean result = (theCoach == alphaCoach);
        System.out.println("pointing to the same objects: " + result);

        System.out.println("the memory location for theCoach: " + theCoach);
        System.out.println("the memory location for alphaCoach" + alphaCoach);

        // close context
        context.close();
    }
}
