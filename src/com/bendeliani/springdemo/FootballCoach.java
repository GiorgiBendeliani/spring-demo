package com.bendeliani.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by BG on 20-Feb-17.
 */

@Component
public class FootballCoach implements Coach {

    @Qualifier("happyFortuneService")
    private FortuneService fortuneService;

    @Autowired
    /*public FootballCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }*/

    public FootballCoach(){
        System.out.println("inside footballCoach default constructor");
    }

    @Override
    public String getDailyWorkout() {
        return "Run 5 km";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getDailyFortune();
    }
}
