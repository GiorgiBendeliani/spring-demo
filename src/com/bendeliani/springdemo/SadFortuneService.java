package com.bendeliani.springdemo;

/**
 * Created by BG on 21-Feb-17.
 */
public class SadFortuneService implements FortuneService {
    @Override
    public String getDailyFortune() {
        return "Today is a sad day";
    }
}
